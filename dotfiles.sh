#!/bin/bash

# FILES
read -p "Copy config files ? (y/n): " repcopy
if [ "$repcopy" = "y" ]
then
	cp ~/.conkyrc ~/Documents/Fedoracula/home/.conkyrc
	cp ~/.zshrc ~/Documents/Fedoracula/home/.zshrc
	cp ~/.config/neofetch/config.conf ~/Documents/Fedoracula/.config/neofetch/config.conf
	cp ~/.config/rofi/config.rasi ~/Documents/Fedoracula/.config/rofi/config.rasi
    cp -avr ~/.themes ~/Documents/Fedoracula/.themes
    cp -avr ~/.local/share/xed/styles/ ~/Documents/Fedoracula/.local/share/xed/styles/
	cp ~/bin/xfcepanel-cpu.sh ~/Documents/Fedoracula/bin/xfcepanel-cpu.sh
	cp ~/bin/xfcepanel-mem.sh ~/Documents/Fedoracula/bin/xfcepanel-mem.sh
	cp ~/bin/xfcepanel-hdd.sh ~/Documents/Fedoracula/bin/xfcepanel-hdd.sh
	cp ~/bin/termcolors-light.sh ~/Documents/Fedoracula/bin/termcolors-light.sh
	sleep 1s
	cd ~/Documents/Fedoracula/ ; echo -e "List:" "\033[0;34m" `la` "\033[0m"
else
	exit
fi

# COMMIT / PUSH

read -p "Push to Git ? (y/n): " reppush
if [ "$reppush" = "y" ]
then
	read -r -p "Message:" gitdesc
	cd ~/Documents/Fedoracula/ ; git add . ; git commit -m "$gitdesc" ; git push -u origin master
	sleep 1s
else
	sleep 1s
fi
