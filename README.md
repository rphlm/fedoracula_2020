Rphl "Fedoracula" XFCE setup.
- Modified Dracula GTK theme
- "Borders Only" Xfwm theme
- Modified Dracula Rofi theme
- XFCE top panel setup & scripts
- Colorschemes
- Apps
- Wallpaper

![Preview](preview1.png "Preview")
![Preview](preview2.png "Preview")
